
//Crea una constante que guarda la conexión con la base de datos.
const dbConnection = require('./connection')
//esencial para la encriptación de la clave.
const bcrypt = require('bcrypt')

//exporta el módulo App
module.exports = app => {
    const connection = dbConnection

    app.post('/register', (req, res) => { 
        const {
            nombre,
            apellido,
            email,
            telefono,
            direccion,
            postal
        } = req.body
        //encripta la clave ingresada por el usuario
        const password = bcrypt.hashSync(req.body.password, 8)
        //inserta las constante ingresadas por el usuario dentro de la BD.
        connection.query('INSERT INTO usuario SET ?', {
            nombre, apellido, email,telefono,direccion,postal, password
        }, (err, result) => {
            res.redirect('/login')//redirige al lógin
        })
    })
}